import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter_smartlook/wireframe/element_data.dart';
import 'package:flutter_smartlook/wireframe/element_descriptors/utils.dart';

class CustomCanvas implements Canvas {
  final Function(Rect, Color?) addSkeleton;
  final PictureRecorder recorder;
  late final Canvas canvas;
  CustomCanvas(
    this.recorder,
    this.addSkeleton,
  ) {
    canvas = Canvas(recorder);
  }

  @override
  void drawRect(Rect rect, Paint paint) async {
    canvas.drawRect(rect, paint);
    addSkeleton(rect, paint.color);
    if (paint.shader is Gradient) {
      final Color? color = await getColorFromPaint(paint);
      addSkeleton(rect, color);
    }
  }

  @override
  void drawCircle(Offset c, double radius, Paint paint) {
    canvas.drawCircle(c, radius, paint);
    addSkeleton(Rect.fromLTWH(c.dx, c.dy, radius, radius), paint.color);
  }

  @override
  void drawArc(
    Rect rect,
    double startAngle,
    double sweepAngle,
    bool useCenter,
    Paint paint,
  ) {
    canvas.drawArc(rect, startAngle, sweepAngle, useCenter, paint);
    addSkeleton(rect, paint.color);
  }

  @override
  void drawPath(Path path, Paint paint) {
    canvas.drawPath(path, paint);
    if (paint.style == PaintingStyle.fill) {
      addSkeleton(path.getBounds(), paint.color);
    } else {
      final List<Skeleton>? skeletons = WireframeUtils.skeletonsFromBorderRect(
        path.getBounds(),
        paint.color,
        paint.strokeWidth,
      );
      if (skeletons == null) {
        return;
      }
      for (final Skeleton skeleton in skeletons) {
        addSkeleton(skeleton.rect, skeleton.color);
      }
    }
  }

  @override
  void drawLine(Offset p1, Offset p2, Paint paint) {
    canvas.drawLine(p1, p2, paint);
    addSkeleton(
      Rect.fromLTWH(
        0,
        0,
        max(p1.dx, p2.dx) - min(p1.dx, p2.dx),
        max(p1.dy, p2.dy) -
            min(
              p1.dy,
              p2.dy,
            ),
      ),
      paint.color,
    );
  }

  Color mixColors(List<Color> colors) {
    final int colorCount = colors.length;

    double red = 0;
    double green = 0;
    double blue = 0;
    double alpha = 0;

    for (final Color color in colors) {
      red += color.red;
      green += color.green;
      blue += color.blue;
      alpha += color.alpha;
    }

    return Color.fromARGB(
      (alpha / colorCount).round(),
      (red / colorCount).round(),
      (green / colorCount).round(),
      (blue / colorCount).round(),
    );
  }

  Future<Color?> getColorFromPaint(Paint paint) async {
    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    canvas.save();
    canvas.drawPaint(paint);
    canvas.restore();
    final picture = pictureRecorder.endRecording();
    final Image image = await picture.toImage(2, 2);
    final byteData = await image.toByteData();

    if (byteData == null) {
      return null;
    }

    return WireframeUtils.createColorFromByteData(
      byteData,
      image.height,
      image.width,
    );
  }

  @override
  void clipPath(Path path, {bool doAntiAlias = true}) {
    canvas.clipPath(path, doAntiAlias: doAntiAlias);
  }

  @override
  void clipRRect(RRect rrect, {bool doAntiAlias = true}) {
    canvas.clipRRect(rrect, doAntiAlias: doAntiAlias);
  }

  @override
  void clipRect(Rect rect,
      {ClipOp clipOp = ClipOp.intersect, bool doAntiAlias = true}) {
    canvas.clipRect(rect, clipOp: clipOp, doAntiAlias: doAntiAlias);
  }

  @override
  void drawAtlas(Image atlas, List<RSTransform> transforms, List<Rect> rects,
      List<Color>? colors, BlendMode? blendMode, Rect? cullRect, Paint paint) {
    canvas.drawAtlas(
      atlas,
      transforms,
      rects,
      colors,
      blendMode,
      cullRect,
      paint,
    );
  }

  @override
  void drawColor(Color color, BlendMode blendMode) {
    canvas.drawColor(color, blendMode);
  }

  @override
  void drawDRRect(RRect outer, RRect inner, Paint paint) {
    canvas.drawDRRect(outer, inner, paint);
  }

  @override
  void drawImage(Image image, Offset offset, Paint paint) {
    canvas.drawImage(image, offset, paint);
  }

  @override
  void drawImageNine(Image image, Rect center, Rect dst, Paint paint) {
    canvas.drawImageNine(image, center, dst, paint);
  }

  @override
  void drawImageRect(Image image, Rect src, Rect dst, Paint paint) {
    canvas.drawImageRect(image, src, dst, paint);
  }

  @override
  void drawOval(Rect rect, Paint paint) {
    canvas.drawOval(rect, paint);
  }

  @override
  void drawPaint(Paint paint) {
    canvas.drawPaint(paint);
  }

  @override
  void drawParagraph(Paragraph paragraph, Offset offset) {
    canvas.drawParagraph(paragraph, offset);
  }

  @override
  void drawPicture(Picture picture) {
    canvas.drawPicture(picture);
  }

  @override
  void drawPoints(PointMode pointMode, List<Offset> points, Paint paint) {
    canvas.drawPoints(pointMode, points, paint);
  }

  @override
  void drawRRect(RRect rrect, Paint paint) {
    canvas.drawRRect(rrect, paint);
  }

  @override
  void drawRawAtlas(Image atlas, Float32List rstTransforms, Float32List rects,
      Int32List? colors, BlendMode? blendMode, Rect? cullRect, Paint paint) {
    canvas.drawRawAtlas(
        atlas, rstTransforms, rects, colors, blendMode, cullRect, paint);
  }

  @override
  void drawRawPoints(PointMode pointMode, Float32List points, Paint paint) {
    canvas.drawRawPoints(pointMode, points, paint);
  }

  @override
  void drawShadow(
      Path path, Color color, double elevation, bool transparentOccluder) {
    canvas.drawShadow(path, color, elevation, transparentOccluder);
  }

  @override
  void drawVertices(Vertices vertices, BlendMode blendMode, Paint paint) {
    canvas.drawVertices(vertices, blendMode, paint);
  }

  @override
  Rect getDestinationClipBounds() {
    return canvas.getDestinationClipBounds();
  }

  @override
  Rect getLocalClipBounds() {
    return canvas.getLocalClipBounds();
  }

  @override
  int getSaveCount() {
    return canvas.getSaveCount();
  }

  @override
  Float64List getTransform() {
    return canvas.getTransform();
  }

  @override
  void restore() {
    canvas.restore();
  }

  @override
  void restoreToCount(int count) {
    canvas.restoreToCount(count);
  }

  @override
  void rotate(double radians) {
    canvas.rotate(radians);
  }

  @override
  void save() {
    canvas.save();
  }

  @override
  void saveLayer(Rect? bounds, Paint paint) {
    canvas.saveLayer(bounds, paint);
  }

  @override
  void scale(double sx, [double? sy]) {
    canvas.scale(sx, sy);
  }

  @override
  void skew(double sx, double sy) {
    canvas.skew(sx, sy);
  }

  @override
  void transform(Float64List matrix4) {
    canvas.transform(matrix4);
  }

  @override
  void translate(double dx, double dy) {
    canvas.translate(dx, dy);
  }
}
