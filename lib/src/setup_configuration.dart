import 'package:flutter_smartlook/src/const_channels.dart';

/// Additional configurations.
class SmartlookSetupConfiguration {
  /// Sets relay proxy host
  Future<void> setRelayProxyHost(String relayProxyHost) async {
    await Channels.channel.invokeMethodOnMobile<void>('setRelayProxyHost', {
      "relayProxyHost": relayProxyHost,
    });
  }
}
